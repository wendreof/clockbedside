package com.example.wlf.relogiodecabeceira;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ViewHolder mViewHolder = new ViewHolder();
    private Handler mHandler = new Handler();
    private Runnable mRunnable;
    private boolean mRunnableStoped = false;
    private boolean mIsBatteryOn = true;

    private BroadcastReceiver mBatteryReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive( Context context, Intent intent )   //ouvir mudanças na bateria
        {
            int lvl = intent.getIntExtra( BatteryManager.EXTRA_LEVEL, 0 );
            mViewHolder.mBatteryLvl.setText( String.valueOf(lvl) + "%" );
        }
    };

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_main );

        this.mViewHolder.mTextHourMinute = this.findViewById( R.id.text_hour_minute );
        this.mViewHolder.mTextSeconds = this.findViewById( R.id.text_seconds );
        this.mViewHolder.mCheckBattery = this.findViewById( R.id.check_battery );
        this.mViewHolder.mBatteryLvl = this.findViewById( R.id.textBatteryLvl );
        this.mViewHolder.mImageOption = this.findViewById( R.id.image_option );
        this.mViewHolder.mImageClose = this.findViewById( R.id.image_close );
        this.mViewHolder.mLinear = this.findViewById( R.id.linear_options );

        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON ); //força a tela a ficar ligada

        getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN ); // NÃO exibe a barra superior do android

        this.registerReceiver( this.mBatteryReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED) );

        this.mViewHolder.mCheckBattery.setChecked( true );

        this.mViewHolder.mLinear.animate().translationY( 500 );

        this.setListener();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        this.mRunnableStoped = false;
        this.startBedSide();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        this.mRunnableStoped = true;
    }

    @Override
    public void onClick( View v )
    {
        int id = v.getId();

        if ( id == R.id.check_battery )
        {
            this.toogleCheckBattery();
        }
        else if( id == R.id.image_option )
        {
            this.mViewHolder.mLinear.setVisibility( View.VISIBLE );
            this.mViewHolder.mLinear.animate()
            .translationY( 0 )
            .setDuration( getResources().getInteger(android.R.integer.config_mediumAnimTime) );
        }
        else if( id == R.id.image_close )
        {
            this.mViewHolder.mLinear.animate()
            .translationY( this.mViewHolder.mLinear.getMeasuredHeight() )
            .setDuration( getResources().getInteger(android.R.integer.config_mediumAnimTime) );
        }
    }

    private void toogleCheckBattery()
    {
        if ( this.mIsBatteryOn )
        {
            this.mIsBatteryOn = false;
            this.mViewHolder.mBatteryLvl.setVisibility( View.GONE );
        }
        else
        {
            this.mViewHolder.mBatteryLvl.setVisibility( View.VISIBLE );
        }
    }

    private void setListener()
    {
        this.mViewHolder.mCheckBattery.setOnClickListener( this );
        this.mViewHolder.mImageOption.setOnClickListener( this );
        this.mViewHolder.mImageClose.setOnClickListener( this );
    }

    private void startBedSide()
    {
        final   Calendar calendar = Calendar.getInstance();
        this.mRunnable = new Runnable()
        {
            @Override
            public void run()
            {

                if ( mRunnableStoped ){  //sai da execução caso a trhead esteja parada
                    return;
                }

                calendar.setTimeInMillis( System.currentTimeMillis() ); //inicia o calenário

                String hourMinutesFormat = String.format(
                        "%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), // pega a hora do dia
                        calendar.get(Calendar.MINUTE)                    // pega o minuto
                );

                String secondsFormat = String.format( "%02d", calendar.get(Calendar.SECOND) );

                mViewHolder.mTextHourMinute.setText( hourMinutesFormat ); // atribuição
                mViewHolder.mTextSeconds.setText( secondsFormat );        // de valores

                long now = SystemClock.uptimeMillis();      // calculo
                long next = now + ( 1000 - (now % 1000) );  // para a proxima execução

                mHandler.postAtTime( mRunnable, next );  // orquestração do Runnable
            }
        };

        this.mRunnable.run();
    }

    private static class ViewHolder
    {
        TextView mTextHourMinute;
        TextView mTextSeconds;
        CheckBox mCheckBattery;
        TextView mBatteryLvl;
        ImageView mImageOption;
        ImageView mImageClose;
        LinearLayout mLinear;
    }
}
